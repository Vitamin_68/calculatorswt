package com.luxoft.vmosin;

public final class MathUtils {

	private MathUtils() {
	}

	public static String mathCalculation(String leftField, String rightField, String mathAction) {
		double result;
		double op1 = Double.parseDouble(leftField);
		double op2 = Double.parseDouble(rightField);
		switch (mathAction) {
		case "+":
			result = op1 + op2;
			break;
		case "-":
			result = op1 - op2;
			break;
		case "*":
			result = op1 * op2;
			break;
		case "/":
			result = op1 / op2;
			break;
		default:
			return "";
		}
		return Double.toString(result);
	}

	public static String newRecordToHystory(String arg1, String mathAction, String arg2, String result) {
		return String.format("%s%s%s=%s", arg1, mathAction, arg2, result);
	}
}
