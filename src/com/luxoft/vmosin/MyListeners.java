package com.luxoft.vmosin;

import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

public final class MyListeners {

	private MyListeners() {
	}

	private static KeyAdapter keyListenerOnFly;
	private static VerifyListener inputTextFieldListener;
	private static SelectionAdapter checkBoxCalcAdapter;
	private static SelectionAdapter clearHistoryListener;
	private static SelectionAdapter resultButtonListener;

	public static VerifyListener getinputTextFieldListener(Text inputTextField) {
		if (inputTextFieldListener == null) {
			inputTextFieldListener = new VerifyListener() {

				@Override
				public void verifyText(VerifyEvent e) {
					StringBuilder builder = new StringBuilder(inputTextField.getText());
					builder.insert(inputTextField.getCaretPosition(), e.text);

					if (!e.text.matches("[0-9.-]*") || !builder.toString().matches("^[-]?\\d*\\.?\\d*$")) {
						e.doit = false;
					}
				}
			};
		}
		return inputTextFieldListener;
	}

	public static SelectionAdapter addClearHistoryListener(List listHist, SashForm sashForm) {
		if (clearHistoryListener == null) {
			clearHistoryListener = new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					listHist.removeAll();
					sashForm.layout();
				}
			};
		}
		return clearHistoryListener;
	}

	public static SelectionAdapter addResultButtonListener(Text arg1, Combo mathAction, Text arg2, Label result,
			Group groupCalc, List listHist) {
		if (resultButtonListener == null) {
			resultButtonListener = new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					calculateAndSaveHistory(arg1.getText(), mathAction.getText(), arg2.getText(), result, groupCalc,
							listHist);
				}
			};
		}
		return resultButtonListener;
	}

	private static void calculateAndSaveHistory(String arg1, String mathAction, String arg2, Label result,
			Group groupCalc, List listHist) {
		if (isValidField(arg1) && isValidField(arg2)) {
			result.setText(calculateResult(arg1, mathAction, arg2));
			addHistory(arg1, mathAction, arg2, result, groupCalc, listHist);
		}
	}

	private static boolean isValidField(String str) {
		if (!str.isEmpty() && !str.equals(".") && !str.equals("-") && !str.equals("-.")
				&& !(Double.parseDouble(str) == 0)) {
			return true;
		}
		return false;
	}

	private static void addHistory(String arg1, String mathAction, String arg2, Label result, Group groupCalc,
			List listHist) {
		listHist.add(MathUtils.newRecordToHystory(arg1, mathAction, arg2, result.getText()));
		groupCalc.layout();
	}

	private static String calculateResult(String arg1, String mathAction, String arg2) {
		return MathUtils.mathCalculation(arg1, arg2, mathAction);
	}

	public static SelectionAdapter getCheckBoxCalcAdapter(Text inputFieldLeft, Combo mathAction, Text inputFieldRight,
			Label result, Group groupCalc, List listHist, Button resultButton, Button checkBoxCalc) {
		if (checkBoxCalcAdapter == null) {
			checkBoxCalcAdapter = new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					if (checkBoxCalc.getSelection()) {
						resultButton.setEnabled(false);
						addListenerOnFlyInputData(inputFieldLeft, mathAction, inputFieldRight, result, groupCalc,
								listHist);
					} else {
						resultButton.setEnabled(true);
						removeListenerOnFlyInputData(inputFieldLeft, mathAction, inputFieldRight, result, groupCalc,
								listHist);
					}
				}
			};
		}
		return checkBoxCalcAdapter;
	}

	private static void removeListenerOnFlyInputData(Text inputFieldLeft, Combo mathAction, Text inputFieldRight,
			Label result, Group groupCalc, List listHist) {
		inputFieldLeft.removeKeyListener(
				getKeyListener(inputFieldLeft, mathAction, inputFieldRight, result, groupCalc, listHist));
		inputFieldRight.removeKeyListener(
				getKeyListener(inputFieldLeft, mathAction, inputFieldRight, result, groupCalc, listHist));
	}

	private static void addListenerOnFlyInputData(Text inputFieldLeft, Combo mathAction, Text inputFieldRight,
			Label result, Group groupCalc, List listHist) {

		inputFieldLeft.addKeyListener(
				getKeyListener(inputFieldLeft, mathAction, inputFieldRight, result, groupCalc, listHist));
		inputFieldRight.addKeyListener(
				getKeyListener(inputFieldLeft, mathAction, inputFieldRight, result, groupCalc, listHist));
	}

	private static KeyAdapter getKeyListener(Text arg1, Combo mathAction, Text arg2, Label result, Group groupCalc,
			List listHist) {
		if (keyListenerOnFly == null) {
			keyListenerOnFly = new KeyAdapter() {

				@Override
				public void keyReleased(KeyEvent e) {
					calculateAndSaveHistory(arg1.getText(), mathAction.getText(), arg2.getText(), result, groupCalc,
							listHist);
				}
			};
		}
		return keyListenerOnFly;
	}
}
