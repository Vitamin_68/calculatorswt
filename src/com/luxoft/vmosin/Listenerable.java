package com.luxoft.vmosin;

import org.eclipse.swt.widgets.Text;

public interface Listenerable {

	default void addListenerInputTextField(Text inputTextField) {
		inputTextField.addVerifyListener(MyListeners.getinputTextFieldListener(inputTextField));
	}

	void addListenerClearHistory();

	void addListenerCalculateOnFly();

	void addListenerResultButton();
}
