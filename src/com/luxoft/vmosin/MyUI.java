package com.luxoft.vmosin;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

public class MyUI implements Listenerable {
	Display display;
	Shell shell;
	Label result;
	List listHist;
	Text inputOne, inputTwo;
	Button checkBoxCalc, resultButton, clearButton;
	Combo mathAction;
	SashForm sashForm;
	Group groupCalc;

	MyUI(Display display, Shell shell) {
		this.display = display;
		this.shell = shell;
	}

	void drawShell() {
		shell.setText("SWT Calculator");
		shell.setSize(420, 450);
		shell.setLayout(new FillLayout());

		TabFolder tabFolder = new TabFolder(shell, SWT.BORDER);

		// tab Calculator
		TabItem tabCalc = new TabItem(tabFolder, SWT.NONE);
		tabCalc.setText("Calculator");
		groupCalc = new Group(tabFolder, SWT.NONE);
		groupCalc.setBackground(new Color(Display.getCurrent(), 245, 245, 245));

		inputOne = createField(30, 30, 120, 30, groupCalc);

		mathAction = new Combo(groupCalc, SWT.READ_ONLY);
		mathAction.setItems(new String[] { "+", "-", "*", "/" });
		mathAction.setText("+");
		mathAction.setBounds(170, 30, 50, 30);
		mathAction.setBackground(new Color(Display.getCurrent(), 255, 255, 255));

		inputTwo = createField(230, 30, 120, 30, groupCalc);

		checkBoxCalc = new Button(groupCalc, SWT.CHECK);
		checkBoxCalc.setBounds(30, 180, 150, 20);
		checkBoxCalc.setText("Calculate on the fly");

		resultButton = new Button(groupCalc, SWT.NONE);
		resultButton.setText("Calculate");
		resultButton.setBounds(240, 175, 110, 30);

		Label label = new Label(groupCalc, SWT.NONE);
		label.setText("Result:");
		label.setBounds(30, 250, 50, 30);

		result = new Label(groupCalc, SWT.BORDER | SWT.RIGHT);
		result.setText("0.0");
		result.setBounds(80, 250, 270, 30);
		result.setBackground(new Color(Display.getCurrent(), 255, 255, 255));

		Label bottomLabel = new Label(groupCalc, SWT.NONE);
		bottomLabel.setBounds(0, 335, 389, 30);
		bottomLabel.setBackground(new Color(Display.getCurrent(), 230, 230, 230));

		tabCalc.setControl(groupCalc);

		// tab History
		TabItem tabHist = new TabItem(tabFolder, SWT.NONE);
		tabHist.setText("History");

		sashForm = new SashForm(tabFolder, SWT.VERTICAL);
		listHist = new List(sashForm, SWT.NONE | SWT.V_SCROLL);

		clearButton = new Button(sashForm, SWT.NONE);
		clearButton.setText("Clear History");

		tabHist.setControl(sashForm);
		sashForm.setWeights(new int[] { 10, 1 });

		addListenerInputTextField(inputOne);
		addListenerInputTextField(inputTwo);
		addListenerClearHistory();
		addListenerCalculateOnFly();
		addListenerResultButton();

		inputOne.setFocus();
		tabFolder.pack();
		shell.setLocation(display.getBounds().width / 2 - shell.getBounds().height / 2,
				display.getBounds().height / 2 - shell.getBounds().width / 2);
		shell.pack();
		shell.open();
	}

	private Text createField(int x, int y, int width, int height, Group group) {
		Text text = new Text(group, SWT.RIGHT | SWT.BORDER);
		text.setTextLimit(20);
		text.setBounds(x, y, width, height);
		return text;
	}

	@Override
	public void addListenerCalculateOnFly() {
		checkBoxCalc.addSelectionListener(MyListeners.getCheckBoxCalcAdapter(inputOne, mathAction, inputTwo, result,
				groupCalc, listHist, resultButton, checkBoxCalc));
	}

	@Override
	public void addListenerClearHistory() {
		clearButton.addSelectionListener(MyListeners.addClearHistoryListener(listHist, sashForm));
	}

	@Override
	public void addListenerResultButton() {
		resultButton.addSelectionListener(
				MyListeners.addResultButtonListener(inputOne, mathAction, inputTwo, result, groupCalc, listHist));
	}
}
