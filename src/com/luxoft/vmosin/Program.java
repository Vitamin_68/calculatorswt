package com.luxoft.vmosin;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Program {

	public static void main(String[] args) {

		Display display = new Display();
		Shell shell = new Shell(display, SWT.DIALOG_TRIM);

		MyUI myUI = new MyUI(display, shell);
		myUI.drawShell();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		shell.dispose();
		display.dispose();
	}
}